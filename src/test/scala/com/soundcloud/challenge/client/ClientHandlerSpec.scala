package com.soundcloud.challenge.client

import akka.actor.ActorSystem
import akka.io.Tcp.{Closed, Received, Write}
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ClientHandlerSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("client-handler-spec"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A ClientHandler" should {

    "register and unregister client" in {
      val handler = system.actorOf(ClientHandler.props(testActor, testActor))
      handler ! Received(ByteString("1"))
      expectMsg(RegisterClient(1))
      handler ! Closed
      expectMsg(UnregisterClient(1))
    }

    "send payload to client" in {
      val handler = system.actorOf(ClientHandler.props(testActor, testActor))

      handler ! Received(ByteString("1"))
      expectMsg(RegisterClient(1))

      val payload = ByteString("666|F|60|50\n")
      handler ! payload
      expectMsg(Write(payload))
    }

  }

}
