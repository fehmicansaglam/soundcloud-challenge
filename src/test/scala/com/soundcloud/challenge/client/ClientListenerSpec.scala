package com.soundcloud.challenge.client

import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.io.Tcp.{Connected, Register}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ClientListenerSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("client-listener-spec"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A ClientListener" should {

    "register a handler when a user client connected" in {
      val listener = system.actorOf(ClientListener.props("localhost", 9099, testActor))
      listener ! Connected(new InetSocketAddress(5555), new InetSocketAddress(9099))
      expectMsgType[Register]
    }

  }

}
