package com.soundcloud.challenge.client

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.ByteString
import com.soundcloud.challenge.event.{StatusUpdate, Broadcast, Follow}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ClientRegistrySpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("client-registry-spec"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A ClientRegistry" should {

    "forward Follow event" in {
      val registry = system.actorOf(ClientRegistry.props)
      registry ! RegisterClient(1)

      val payload = ByteString("1|F|2|1")
      registry ! Follow(payload, 1, 2, 1)
      expectMsg(payload)
    }

    "forward Broadcast event" in {
      val registry = system.actorOf(ClientRegistry.props)
      registry ! RegisterClient(1)
      registry ! RegisterClient(2)

      val payload = ByteString("1|B")
      registry ! Broadcast(payload, 1)
      expectMsg(payload)
      expectMsg(payload)
    }

    "forward PrivateMessage event" in {
      val registry = system.actorOf(ClientRegistry.props)
      registry ! RegisterClient(56)

      val payload = ByteString("3|P|32|56")
      registry ! Follow(payload, 3, 32, 56)
      expectMsg(payload)
    }

    "forward StatusUpdate event" in {
      val registry = system.actorOf(ClientRegistry.props)

      // we are the client for 2
      registry ! RegisterClient(2)

      // 2 follows 1
      registry ! Follow(ByteString("1|F|2|1"), 1, 2, 1)

      // 1 updates their status
      val payload = ByteString("2|S|1")
      registry ! StatusUpdate(payload, 2, 1)

      // 2 should be notified
      expectMsg(payload)
    }

  }

}
