package com.soundcloud.challenge.event

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class EventExtractorSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("event-extractor-spec"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An EventExtractor" should {

    "extract Follow event" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      val payload = ByteString("666|F|60|50\n")
      eventExtractor ! payload
      expectMsg(Follow(payload, 666, 60, 50))
    }

    "extract Unfollow event" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      val payload = ByteString("1|U|12|9\n")
      eventExtractor ! payload
      expectMsg(Unfollow(payload, 1, 12, 9))
    }

    "extract Broadcast event" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      val payload = ByteString("542532|B\n")
      eventExtractor ! payload
      expectMsg(Broadcast(payload, 542532))
    }

    "extract PrivateMessage event" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      val payload = ByteString("43|P|32|56\n")
      eventExtractor ! payload
      expectMsg(PrivateMessage(payload, 43, 32, 56))
    }

    "extract StatusUpdate event" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      val payload = ByteString("634|S|32\n")
      eventExtractor ! payload
      expectMsg(StatusUpdate(payload, 634, 32))
    }

    "extract batch events" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      val payloadF = ByteString("666|F|60|50\n")
      val payloadU = ByteString("1|U|12|9\n")
      val payloadB = ByteString("542532|B\n")
      val payloadP = ByteString("43|P|32|56\n")
      val payloadS = ByteString("634|S|32\n")
      eventExtractor ! (payloadF ++ payloadU ++ payloadB ++ payloadP ++ payloadS)
      expectMsg(BulkEvents(List(
        Follow(payloadF, 666, 60, 50),
        Unfollow(payloadU, 1, 12, 9),
        Broadcast(payloadB, 542532),
        PrivateMessage(payloadP, 43, 32, 56),
        StatusUpdate(payloadS, 634, 32))))
    }

    "concatenate partial payloads and extract events" in {
      val eventExtractor = system.actorOf(EventExtractor.props)
      eventExtractor ! ByteString("666|F|60|50\n1|U")
      eventExtractor ! ByteString("|12|9\n542532|B\n")
      expectMsg(Follow(ByteString("666|F|60|50\n"), 666, 60, 50))
      expectMsg(BulkEvents(List(
        Unfollow(ByteString("1|U|12|9\n"), 1, 12, 9),
        Broadcast(ByteString("542532|B\n"), 542532))))
    }

  }

}
