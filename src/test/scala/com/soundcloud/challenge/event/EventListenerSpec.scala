package com.soundcloud.challenge.event

import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.io.Tcp.{Connected, Register}
import akka.testkit.{ImplicitSender, TestKit}
import com.soundcloud.challenge.client.ClientRegistry
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class EventListenerSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("event-listener-spec"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An EventListener" should {

    "register a handler when an event source connected" in {
      val listener = system.actorOf(EventListener.props("localhost", 9090, testActor))
      listener ! Connected(new InetSocketAddress(5555), new InetSocketAddress(9090))
      expectMsgType[Register]
    }

  }

}
