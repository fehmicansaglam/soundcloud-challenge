package com.soundcloud.challenge.event

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class EventHandlerSpec(_system: ActorSystem)
  extends TestKit(_system)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("event-handler-spec"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An EventHandler" should {

    "process events in correct order" in {
      val handler = system.actorOf(EventHandler.props(testActor))

      val payloadF = ByteString("1|F|60|50\n")
      val payloadU = ByteString("2|U|12|9\n")
      val payloadB = ByteString("3|B\n")
      val payloadP = ByteString("4|P|32|56\n")
      val payloadS = ByteString("5|S|32\n")

      handler ! Broadcast(payloadB, 3)
      handler ! Unfollow(payloadU, 2, 12, 9)
      handler ! StatusUpdate(payloadS, 5, 32)
      handler ! Follow(payloadF, 1, 60, 50)
      handler ! PrivateMessage(payloadP, 4, 32, 56)

      expectMsg(Follow(payloadF, 1, 60, 50))
      expectMsg(Unfollow(payloadU, 2, 12, 9))
      expectMsg(Broadcast(payloadB, 3))
      expectMsg(PrivateMessage(payloadP, 4, 32, 56))
      expectMsg(StatusUpdate(payloadS, 5, 32))
    }

    "process bulk events in correct order" in {
      val handler = system.actorOf(EventHandler.props(testActor))

      val payloadF = ByteString("1|F|60|50\n")
      val payloadU = ByteString("2|U|12|9\n")
      val payloadB = ByteString("3|B\n")
      val payloadP = ByteString("4|P|32|56\n")
      val payloadS = ByteString("5|S|32\n")

      handler ! BulkEvents(List(
        Broadcast(payloadB, 3),
        Unfollow(payloadU, 2, 12, 9),
        StatusUpdate(payloadS, 5, 32),
        Follow(payloadF, 1, 60, 50),
        PrivateMessage(payloadP, 4, 32, 56)))

      expectMsg(Follow(payloadF, 1, 60, 50))
      expectMsg(Unfollow(payloadU, 2, 12, 9))
      expectMsg(Broadcast(payloadB, 3))
      expectMsg(PrivateMessage(payloadP, 4, 32, 56))
      expectMsg(StatusUpdate(payloadS, 5, 32))
    }

  }

}
