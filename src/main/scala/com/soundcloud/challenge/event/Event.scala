package com.soundcloud.challenge.event

import akka.util.ByteString

sealed trait Event {
  /** Unique sequence number of this event. */
  def seq: Int

  /** Original payload of the event received from the event source. */
  def payload: ByteString
}

object Event {

  /**
   * Parses an event from the given payload. Payload must represent exactly 1 event.
   *
   * @param payload data representing an event
   * @return parsed concrete [[Event]] instance
   */
  def parse(payload: ByteString): Option[Event] = {
    val parts = payload.utf8String.trim.split('|')
    val seq = parts(0).toInt
    parts(1) match {
      case "F" => Some(Follow(payload, seq, parts(2).toInt, parts(3).toInt))
      case "U" => Some(Unfollow(payload, seq, parts(2).toInt, parts(3).toInt))
      case "B" => Some(Broadcast(payload, seq))
      case "P" => Some(PrivateMessage(payload, seq, parts(2).toInt, parts(3).toInt))
      case "S" => Some(StatusUpdate(payload, seq, parts(2).toInt))
      case _ => None
    }
  }

  /** Implicit ordering for events. Events are ordered ascending by their sequnce number */
  implicit val ordering: Ordering[Event] = Ordering.by[Event, Int](_.seq).reverse
}

case class Follow(payload: ByteString, seq: Int, fromId: Int, toId: Int) extends Event

case class Unfollow(payload: ByteString, seq: Int, fromId: Int, toId: Int) extends Event

case class Broadcast(payload: ByteString, seq: Int) extends Event

case class PrivateMessage(payload: ByteString, seq: Int, fromId: Int, toId: Int) extends Event

case class StatusUpdate(payload: ByteString, seq: Int, fromId: Int) extends Event

