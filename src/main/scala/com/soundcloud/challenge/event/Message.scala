package com.soundcloud.challenge.event

sealed trait Message

case class BulkEvents(events: Seq[Event]) extends Message
