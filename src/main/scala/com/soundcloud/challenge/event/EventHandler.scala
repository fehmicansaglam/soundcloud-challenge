package com.soundcloud.challenge.event

import akka.actor._
import akka.io.Tcp.{ConnectionClosed, Received}
import com.soundcloud.challenge.common.PrintStats

import scala.collection.mutable
import scala.concurrent.duration._

/**
 * This actor receives events from an event source. Events which are received as [[akka.util.ByteString]] payloads are
 * extracted and converted to concrete [[Event]] instances using [[EventExtractor]]. Extracted events are then added to
 * a [[mutable.PriorityQueue]] to be sorted by their sequence number. When the head of the queue has the expected
 * sequence number that event is sent to [[com.soundcloud.challenge.client.ClientRegistry]] for taking required actions.
 *
 * @param registry reference to [[com.soundcloud.challenge.client.ClientRegistry]] actor
 */
class EventHandler(registry: ActorRef) extends Actor with ActorLogging {

  import context.dispatcher

  /** Extractor actor to extract events from payloads. */
  private[this] val extractor = context.actorOf(EventExtractor.props, "event-extractor")

  /** Event queue stores the events waiting to be processed which are ordered ascending by sequence number. */
  private[this] val eventQueue = mutable.PriorityQueue.empty[Event]

  /** Sequence number of the last processed event. */
  private[this] var lastEventSeq = 0

  context.system.scheduler.schedule(5.seconds, 5.seconds, self, PrintStats)

  context.watch(extractor)

  def receive: Receive = {
    case Received(payload) => extractor ! payload

    case BulkEvents(events) =>
      eventQueue.enqueue(events: _*)
      processEvents()

    case e: Event =>
      eventQueue.enqueue(e)
      processEvents()

    case _: ConnectionClosed =>
      extractor ! PoisonPill

    case PrintStats =>
      printStats()

    case _: Terminated =>
      // Extractor has been terminated. As we will not receive any new events do not check the sequence number
      // while processing events.
      processEvents(noCheckSeq = true)
      printStats()
      context.stop(self)
  }

  /**
   * Iterates over the event queue and sends each event to Client Registry. Event queue is ordered ascending by
   * sequence number.
   *
   * @param noCheckSeq If true don't check the next event for equality to the expected sequence number.
   *                   Defaults to false.
   */
  private def processEvents(noCheckSeq: Boolean = false): Unit = {
    while (eventQueue.nonEmpty && (noCheckSeq || eventQueue.head.seq == lastEventSeq + 1)) {
      registry ! eventQueue.dequeue()
      lastEventSeq = lastEventSeq + 1
    }
  }

  private def printStats(): Unit = {
    log.info(
      """
        |{} events have been processed so far.
        |There are {} events buffered in the event queue.
      """.stripMargin, lastEventSeq, eventQueue.size)
  }
}

object EventHandler {
  def props(registry: ActorRef): Props = Props(classOf[EventHandler], registry)
}
