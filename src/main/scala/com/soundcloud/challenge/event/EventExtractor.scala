package com.soundcloud.challenge.event

import akka.actor.{Actor, ActorLogging, Props}
import akka.util.ByteString

import scala.annotation.tailrec

/**
 * This actor is responsible for extracting/parsing events from payloads. Received data may belong to partial payloads.
 * EventExtractor concatenates received data and then tries to parse events.
 */
class EventExtractor extends Actor with ActorLogging {

  private[this] var storage = ByteString.empty
  val separator = '\n'.toByte

  def receive: Receive = {
    case data: ByteString =>
      storage ++= data
      val (events, rest) = processEvents(storage)
      events match {
        case Nil =>
        case head :: Nil => sender() ! head
        case _ => sender() ! BulkEvents(events)
      }
      storage = rest
  }

  @tailrec
  private def processEvents(data: ByteString, events: Seq[Event] = Seq.empty): (Seq[Event], ByteString) = {
    val end = data.indexOf(separator)
    if (end == -1) {
      (events, data)
    } else {
      val (event, rest) = data.splitAt(end + 1)
      processEvents(rest, events ++ Event.parse(event))
    }
  }

}

object EventExtractor {
  def props: Props = Props[EventExtractor]
}
