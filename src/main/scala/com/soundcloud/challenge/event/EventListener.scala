package com.soundcloud.challenge.event

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}

/**
 * This is the server actor that event source connects to. It binds to the given host and port and waits for
 * connections. Although practically there will be just one event source, any number of event sources can connect
 * any time.
 *
 * When an event source connects, an [[EventHandler]] actor is registered to the connection and further processing is
 * done by this actor.
 *
 * @param host the host to bind to
 * @param port the port to bind to
 * @param registry reference to a [[com.soundcloud.challenge.client.ClientRegistry]] actor
 */
class EventListener(host: String, port: Int, registry: ActorRef) extends Actor with ActorLogging {

  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(host, port))

  def receive = {
    case b@Bound(localAddress) =>
      log.info("Event listener bound to {}", localAddress)

    case CommandFailed(b: Bind) =>
      log.error("Event listener could not bind to {}", b.localAddress)
      context.system.shutdown()

    case c@Connected(remote, local) =>
      log.info("Event source connected from {}", remote)
      val handler = context.actorOf(EventHandler.props(registry))
      val connection = sender()
      connection ! Register(handler)
  }
}

object EventListener {
  def props(host: String, port: Int, registry: ActorRef): Props = Props(classOf[EventListener], host, port, registry)
}
