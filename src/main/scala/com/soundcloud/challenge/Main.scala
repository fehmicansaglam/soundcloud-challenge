package com.soundcloud.challenge

import akka.actor.ActorSystem
import com.soundcloud.challenge.client.{ClientListener, ClientRegistry}
import com.soundcloud.challenge.event.EventListener

object Main extends App {
  implicit val system = ActorSystem("soundcloud-challenge")

  val eventListenerPort = Option(System.getenv("eventListenerPort")).map(_.toInt).getOrElse(9090)
  val clientListenerPort = Option(System.getenv("clientListenerPort")).map(_.toInt).getOrElse(9099)

  val registry = system.actorOf(ClientRegistry.props, "client-registry")
  system.actorOf(EventListener.props("localhost", eventListenerPort, registry), "event-listener")
  system.actorOf(ClientListener.props("localhost", clientListenerPort, registry), "client-listener")
}
