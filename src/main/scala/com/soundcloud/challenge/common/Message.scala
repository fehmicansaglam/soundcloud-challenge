package com.soundcloud.challenge.common

sealed trait Message

case object PrintStats extends Message
