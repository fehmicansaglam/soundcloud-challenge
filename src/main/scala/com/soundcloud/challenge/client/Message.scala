package com.soundcloud.challenge.client

sealed trait Message

case class RegisterClient(id: Int) extends Message

case class UnregisterClient(id: Int) extends Message
