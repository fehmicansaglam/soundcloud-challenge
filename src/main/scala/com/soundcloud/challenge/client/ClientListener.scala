package com.soundcloud.challenge.client

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}

/**
 * This is the server actor that user clients connect to. It binds to the given host and port and waits for
 * connections. Any number of user clients can connect any time.
 *
 * When a user client connects, the connection is passed to a [[ClientHandler]] actor and further processing is done
 * by this actor.
 *
 * @param host the host to bind to
 * @param port the port to bind to
 * @param registry reference to a [[com.soundcloud.challenge.client.ClientRegistry]] actor
 */
class ClientListener(host: String, port: Int, registry: ActorRef) extends Actor with ActorLogging {

  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(host, port))

  def receive = {
    case Bound(localAddress) =>
      log.info("Client listener bound to {}", localAddress)

    case CommandFailed(b: Bind) =>
      log.error("Client listener could not bind to {}", b.localAddress)
      context.system.shutdown()

    case c@Connected(remote, local) =>
      log.info("User client connected from {}", remote)
      val connection = sender()
      val handler = context.actorOf(ClientHandler.props(connection, registry))
      connection ! Register(handler)
  }
}

object ClientListener {
  def props(host: String, port: Int, registry: ActorRef): Props = Props(classOf[ClientListener], host, port, registry)
}

