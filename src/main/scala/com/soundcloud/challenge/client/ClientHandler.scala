package com.soundcloud.challenge.client

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp.{ConnectionClosed, Received, Write}
import akka.util.ByteString

/**
 * A new ClientHandler is created for each connected user client. When the handler receives the unique user id,
 * it registers itself to [[ClientRegistry]]. After registration, ClientHandler starts forwarding events received from
 * [[ClientRegistry]] to the user client.
 *
 * @param connection reference to the connection actor
 * @param registry reference to [[ClientRegistry]] actor
 */
class ClientHandler(connection: ActorRef, registry: ActorRef) extends Actor with ActorLogging {

  def receive: Receive = {
    case Received(data) =>
      val id = data.utf8String.trim.toInt
      registry ! RegisterClient(id)
      context.become(ready(id))

    case _: ConnectionClosed =>
      context.stop(self)
  }

  def ready(id: Int): Receive = {
    case _: ConnectionClosed =>
      registry ! UnregisterClient(id)
      context.stop(self)

    case data: ByteString => connection ! Write(data)
  }
}

object ClientHandler {
  def props(connection: ActorRef, registry: ActorRef): Props = Props(classOf[ClientHandler], connection, registry)
}
