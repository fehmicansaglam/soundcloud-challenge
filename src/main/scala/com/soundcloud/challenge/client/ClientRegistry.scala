package com.soundcloud.challenge.client

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.soundcloud.challenge.common.PrintStats
import com.soundcloud.challenge.event._

import scala.collection.mutable
import scala.concurrent.duration._

/**
 * Receives events from [[EventHandler]] and takes appropriate actions. According to received events it also tracks
 * followers of users. If an event requires notification of user clients then the event is forwarded to appropriate
 * [[ClientHandler]] actors.
 */
class ClientRegistry extends Actor with ActorLogging {

  import context.dispatcher

  /** Keeps track of the userId, [[ClientHandler]] mappings. */
  private[this] val clients = mutable.Map.empty[Int, ActorRef]

  /** Keeps track of the followers of users. */
  private[this] val followers = mutable.Map.empty[Int, Set[Int]]

  context.system.scheduler.schedule(10.seconds, 10.seconds, self, PrintStats)

  def receive: Receive = {
    case RegisterClient(id) =>
      log.debug("Register ClientHandler of {}", id)
      clients += id -> sender()

    case UnregisterClient(id) =>
      log.debug("Unregister ClientHandler of {}", id)
      clients -= id

    case Follow(payload, _, fromId, toId) =>
      log.debug("{} followed {}", fromId, toId)
      clients.get(toId).foreach(_ ! payload)
      val newFollowers = followers.getOrElseUpdate(toId, Set.empty) + fromId
      followers += toId -> newFollowers

    case Unfollow(payload, _, fromId, toId) =>
      log.debug("{} unfollowed {}", fromId, toId)
      val newFollowers = followers.getOrElseUpdate(toId, Set.empty) - fromId
      followers += toId -> newFollowers

    case Broadcast(payload, _) =>
      clients.foreach { case (_, ref) => ref ! payload }

    case PrivateMessage(payload, _, _, toId) =>
      clients.get(toId).foreach(_ ! payload)

    case StatusUpdate(payload, _, fromId) =>
      followers.get(fromId).foreach { set =>
        set.flatMap(clients.get).foreach(_ ! payload)
      }

    case PrintStats =>
      printStats()
  }

  private def printStats(): Unit = {
    val values = followers.values.filter(_.nonEmpty).map(_.size)
    val (count, min, max, avg) = if (values.isEmpty) {
      (0, 0, 0, 0.0)
    } else {
      (values.size, values.min, values.max, values.sum.toDouble / values.size)
    }

    log.info(
      """
        |There are {} registered user clients.
        |{} user(s) have min:{}, max:{}, avg:{} followers. (Users w/o followers are ignored)
      """.stripMargin, Array(clients.size, count, min, max, avg.formatted("%.2f")))
  }
}

object ClientRegistry {
  def props: Props = Props[ClientRegistry]
}
