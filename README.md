# Follower Maze

This application acts as a socket server, reading events from an event source and forwarding them when appropriate to
user clients.

Clients connect to the server through TCP. There are two types of clients connecting to the server:

- One event source: It connects to the server on port 9090 and sends a stream of events which may or may not require
clients to be notified.

- Many user clients: Each one connects to the server on port 9099 and represents a specific user with a unique id. User
clients wait for notifications for events which would be relevant to the user they represent.

### How do I build and run?

The build system used is SBT. To run the application just go to the project folder and type the commands below in a
terminal window. Application should start in a couple of seconds. Be aware that if you are running an SBT build for the
very first time on your system it may take some minutes to build and run the application.

```
$ cd soundcloud-challenge
$ ./sbt run
```

As soon as the application starts, it binds to ports 9090 and 9099. After that, clients can connect to the server. If
you ever need to change these ports, set the environment variables as described below:

- **eventListenerPort** - Default: 9090

   The port used by the event source.

- **clientListenerPort** - Default: 9099

   The port used to register clients.

### How do I run automated tests?

Automated tests can also be run using SBT. Just type the following command in a terminal window.

```
$ ./sbt test
```

### What languages/libraries are used for development?

This application has been developed using Scala. It has only one dependency, Akka, other than the Scala library and
scalatest which is used for automated testing.

### Why Akka?

Akka is the de facto actor library for Scala. Any system with the need for high-throughput and low latency is a good
candidate for using Akka. Implementation of a high-performance realtime stream processing application with many clients
needs to be asynchronous and non-blocking. Akka enables developing top-down non-blocking applications and Akka IO
enables serving many clients simultaneously over TCP in a non-blocking way.
